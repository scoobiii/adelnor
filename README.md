# adelnor

![rubygems](https://badgen.net/rubygems/n/adelnor)
![rubygems](https://badgen.net/rubygems/v/adelnor/latest)
![rubygems](https://badgen.net/rubygems/dt/adelnor)

![Build](https://github.com/leandronsp/adelnor/actions/workflows/build.yml/badge.svg)
[![Ruby Style Guide](https://img.shields.io/badge/code_style-rubocop-brightgreen.svg)](https://github.com/rubocop/rubocop)
[![Ruby Style Guide](https://img.shields.io/badge/code_style-community-brightgreen.svg)](https://rubystyle.guide)
```
     ___       _______   _______  __      .__   __.   ______   .______      
    /   \     |       \ |   ____||  |     |  \ |  |  /  __  \  |   _  \     
   /  ^  \    |  .--.  ||  |__   |  |     |   \|  | |  |  |  | |  |_)  |    
  /  /_\  \   |  |  |  ||   __|  |  |     |  . `  | |  |  |  | |      /     
 /  _____  \  |  '--'  ||  |____ |  `----.|  |\   | |  `--'  | |  |\  \----.
/__/     \__\ |_______/ |_______||_______||__| \__|  \______/  | _| `._____|
```

[adelnor](https://rubygems.org/gems/adelnor) is a dead simple, yet Rack-compatible, HTTP server written in Ruby.

## Requirements

Ruby

## Installation
```bash
$ gem install adelnor
```

## Development tooling

Make and Docker

## Using make

```bash
$ make help
```
Output:
```
Usage: make <target>
  help                       Prints available commands
  sample.server              Runs a sample server in the port 3000
  bundle.install             Installs the Ruby gems
  bash                       Creates a container Bash
  run.tests                  Runs Unit tests
  rubocop                    Runs code linter
  ci                         Runs Unit tests in CI
  gem.publish                Publishes the gem to https://rubygems.org (auth required)
  gem.yank                   Removes a specific version from the Rubygems
```

## Running a sample server in development

```bash
$ make bundle.install
$ make sample.server
```

## Handling concurrency

Currently Adelnor allows to run the server using different concurrency strategies, one at a time.

```ruby
require './lib/adelnor/server'

app = -> (env) do
  [200, { 'Content-Type' => 'text/html' }, 'Hello world!']
end

# Threaded mode
Adelnor::Server.run app, 3000, thread_pool: 5

# Clusterd mode (forking)
Adelnor::Server.run app, 3000, workers: 2

# Async mode
Adelnor::Server.run app, 3000, async: true
```

Open `http://localhost:3000`

## Evolução do Projeto Adelnor Server
Objetivo:

O objetivo principal do Adelnor Server é ser um servidor de alto desempenho, rápido e distribuído, com as seguintes características:

100% em memória: Todos os dados são armazenados na memória, o que garante alta velocidade de acesso e processamento.
Autoescala vertical e horizontal: O servidor pode aumentar ou diminuir automaticamente a quantidade de recursos alocados de acordo com a demanda.
Distribuído: O servidor pode ser executado em várias máquinas, o que aumenta a capacidade de processamento e a confiabilidade.
Operação em no mínimo 3 regiões GCP: Para garantir a segurança e a disponibilidade dos dados, o Adelnor Server opera em no mínimo 3 regiões (autônomas, GCP, Azuere, IBM, Oracle, Tencent, SAP, Alibaba, o Google Cloud Platform (GCP).
Aplicações ideais:

Com base nessas características, o Adelnor Server é ideal para aplicações que exigem alto desempenho, escalabilidade e confiabilidade, como:

Sistemas de transações financeiras: Processamento de pagamentos, transferências de dinheiro e outras transações financeiras.
Sistemas de jogos online: Suporta jogos online com grande número de jogadores e alta demanda.
Sistemas de análise de dados em tempo real: Processamento e análise de grandes volumes de dados em tempo real.
Estratégias de Evolução:

Existem diversas estratégias para evoluir o Adelnor Server:

1. Aprimorar o desempenho:

Otimizar o código para melhorar a velocidade de processamento.
Implementar técnicas de cache para reduzir o tempo de acesso aos dados.
Investigar e implementar algoritmos mais eficientes.
2. Aumentar a escalabilidade:

Melhorar a capacidade do servidor de lidar com um grande número de requisições simultâneas.
Implementar técnicas de balanceamento de carga para distribuir o tráfego entre várias máquinas.
Otimizar o uso de recursos em diferentes ambientes.
3. Ampliar a funcionalidade:

Adicionar suporte para novos protocolos e tecnologias.
Implementar recursos de segurança e confiabilidade aprimorados.
Integrar com outras ferramentas e serviços.
4. Fortalecer a comunidade:

Criar documentação completa e atualizada.
Oferecer tutoriais e exemplos de uso.
Fornecer suporte técnico e fóruns de discussão.
5. Explorar novas tecnologias:

Investigar e implementar novas tecnologias que possam beneficiar o servidor.
Manter-se atualizado com as últimas tendências do mercado.
Experimentar novas abordagens e soluções inovadoras.
Processo de decisão:

A escolha da melhor estratégia de evolução dependerá de diversos fatores, como:

Necessidades dos usuários: Quais são os principais desafios e demandas da comunidade do Adelnor Server?
Recursos disponíveis: Quais recursos humanos, financeiros e tecnológicos estão disponíveis para o desenvolvimento do projeto?
Prioridades do projeto: Quais são os objetivos de curto, médio e longo prazo para o Adelnor Server?
Recomendações:

Realizar pesquisas e análises para identificar as melhores oportunidades de evolução.
Envolver a comunidade no processo de decisão para garantir que o projeto atenda às suas necessidades.
Priorizar as melhorias que impactam diretamente o desempenho, a escalabilidade e a usabilidade do servidor.
Manter um ciclo de desenvolvimento contínuo para garantir a evolução constante do projeto.
Conclusão:

O Adelnor Server é um projeto com grande potencial e diversas oportunidades de evolução. Ao seguir as estratégias e considerações descritas acima, o projeto poderá se tornar um servidor de alto desempenho, escalável e confiável, capaz de atender às demandas de aplicações complexas e exigentes.
